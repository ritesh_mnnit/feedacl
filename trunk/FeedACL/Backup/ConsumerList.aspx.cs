﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.FeedACLLib.Models;
using SuperSport.FeedACLLib;
using SuperSport.FeedACLLib.DAL;
using FeedACL;

namespace SuperSport.FeedACL
{
    public partial class ConsumerList : System.Web.UI.Page
    {
        private static readonly IAccessControlService accessControl = new AccessControlDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (((SiteMaster)Page.Master) != null)
            {
                ((SiteMaster)Page.Master).PageTitle = "Consumer List";
            }

            RenderConsumers();
        }

        private void RenderConsumers()
        {
            List<Consumer> consumers = accessControl.Get();
            rptrConsumers.DataSource = consumers;
            rptrConsumers.DataBind();
        }
    }
}