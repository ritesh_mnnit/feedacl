﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.FeedACLLib.Models;
using SuperSport.FeedACLLib;
using SuperSport.FeedACLLib.DAL;
using FeedACL;

namespace SuperSport.FeedACL.Controls
{
    public partial class CreateConsumer : System.Web.UI.UserControl
    {
        private static readonly IAccessControlService accessControl = new AccessControlDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (((SiteMaster)Page.Master) != null)
            {
                ((SiteMaster)Page.Master).PageTitle = "Create Consumer";
            }

            if (!Page.IsPostBack)
            {
                string[] names = Enum.GetNames(typeof(METHODS));

                for (int i = 0; i < names.Length; i++)
                {
                    ListItem item = new ListItem(names[i], names[i]);
                    chkMethodAcces.Items.Add(item);
                }
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Consumer c = new Consumer();
            c.Active = rdActive.Checked;
            c.AllowAll = rdAllowAll.Checked;
            c.AuthKey = System.Guid.NewGuid().ToString();
            if (c.MethodAccess == null)
            {
                c.MethodAccess = new List<string>();
            }

            foreach (ListItem cBox in chkMethodAcces.Items)
            {
                if (cBox.Selected)
                {
                    c.MethodAccess.Add(cBox.Value);
                }
            }

            c.Name = txName.Text;
            AccessItem ac = new AccessItem();
            if (c.AccessItems == null)
            {
                c.AccessItems = new List<AccessItem>();
            }

            if (this.Request.Form.Get("txSport") != null)
            {
                string[] sports = this.Request.Form.Get("txSport").Split(',');
                string[] tournaments = this.Request.Form.Get("txTournament").Split(',');
                int i = 0;
                foreach (string s in sports)
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        AccessItem acitem = new AccessItem();
                        acitem.Sport = s;
                        if (!string.IsNullOrEmpty(tournaments[i]))
                        {
                            acitem.Tournament = tournaments[i];
                            c.AccessItems.Add(acitem);
                        }
                    }
                    i++;
                }
            }         

            accessControl.Create(c);
            Response.Redirect("ConsumerList.aspx");
        }
    }
}