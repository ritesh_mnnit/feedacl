﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateConsumer.ascx.cs" Inherits="SuperSport.FeedACL.Controls.CreateConsumer" %>
<div class="padded">
<script type="text/javascript">
 
$j(document).ready(function(){

    var counter = 2;
 
    $j("#addButton").click(function () {
 
	if(counter>10){
            alert("Only 10 textboxes allow");
            return false;
	}   
 
	var newTextBoxDiv = $j(document.createElement('div'))
	     .attr("id", 'TextBoxDiv' + counter);

	newTextBoxDiv.after().html('<span>Sport: </span>' +
	      '<input type="text" name="txSport" id="txSport" value="" >' +
          '<span>Tournament: </span>' +
	      '<input type="text" name="txTournament" id="txTournament" value="" >');
 
	newTextBoxDiv.appendTo("#TextBoxesGroup"); 
 
	counter++;
     });
});
</script>


<div>
    <h3>Consumer Name:</h3>
    <asp:TextBox ID="txName" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="rqField" runat="server" ErrorMessage="* Required" ControlToValidate="txName" CssClass="label red"></asp:RequiredFieldValidator>
</div>


<table class="table table-bordered table-striped table-header">
  <tbody>
  <tr>
    <td>Active</td>
    <td><asp:CheckBox ID="rdActive" Text="" runat="server" /></td>
  </tr>
  <tr>
    <td>Allow All</td>
    <td><asp:CheckBox ID="rdAllowAll" Text="" runat="server" /></td>
  </tr>
  <tr>
    <td>Method Access</td>
    <td><asp:CheckBoxList ID="chkMethodAcces" runat="server">
        </asp:CheckBoxList>
    </td>
  </tr>
  </tbody>
</table>

<h3>access list</h3>
<div id='TextBoxesGroup'>
	<div id="TextBoxDiv1">
    </div>    
</div>    
<input type='button' value='Add' id='addButton' class="btn blue" />

<br />
<br />
<asp:Button ID="btnCreate" runat="server" Text="Create" class="btn green"
    onclick="btnCreate_Click" />
</div>