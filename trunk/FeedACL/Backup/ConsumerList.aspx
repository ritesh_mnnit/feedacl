﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConsumerList.aspx.cs" Inherits="SuperSport.FeedACL.ConsumerList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">



<div class="padded">

<ul class="nav inset-margin pull-right">
    <li>
        <a class="btn blue" href="CreateConsumer.aspx"><i class="icon-plus"></i>Create new consumer</a>
    </li>
</ul>

<%--<a class="btn" data-toggle="modal" href="/CreateConsumerModal.aspx">Launch Modal</a>--%>

<asp:Repeater ID="rptrConsumers" runat="server">
    <HeaderTemplate>
        <table class="table table-bordered table-striped table-header">
          <thead>
          <tr>
            <th>Consumer</th>
            <th>AuthKey</th>
            <th>Allow All</th>
            <th>Active</th>
          </tr>
          </thead>
          <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td><a href="ViewConsumer.aspx?id=<%# Eval("id")%>"><%# Eval("Name")%></a></td>
            <td><%# Eval("AuthKey")%></td>
            <td><%# Eval("AllowAll")%></td>  
            <td>
                <%# (Eval("Active").ToString().Equals("True") ? "<span class=\"btn green mini\">active</span>" : "<span class=\"btn red mini\">inactive</span>")%>
            </td>  
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr>
            <td><a href="ViewConsumer.aspx?id=<%# Eval("id")%>"><%# Eval("Name")%></a></td>
            <td><%# Eval("AuthKey")%></td>
            <td><%# Eval("AllowAll")%></td>  
            <td>
                <%# (Eval("Active").ToString().Equals("True") ? "<span class=\"btn green mini\">active</span>" : "<span class=\"btn red mini\">inactive</span>")%>
            
            </td>  
        </tr>
    </AlternatingItemTemplate>
    <FooterTemplate>
      </tbody>
    </table>
    </FooterTemplate>
</asp:Repeater>
</div>
</asp:Content>