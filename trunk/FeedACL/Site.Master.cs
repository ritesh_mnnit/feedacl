﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace FeedACL
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {

        public string PageTitle { get; set; }

        protected override void OnInit(EventArgs e)
        {
            if (!Request.Url.ToString().Contains("default.aspx"))
            {
                if (Request.Cookies.Get(ConfigurationManager.AppSettings["PrimaryCookie"]) == null)
                {
                    Response.Redirect("default.aspx");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //ltrlHeading.Text = this.PageTitle;
        }
    }
}
