﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.FeedACLLib.Models;
using SuperSport.FeedACLLib;
using SuperSport.FeedACLLib.DAL;
using FeedACL;

namespace SuperSport.FeedACL.Controls
{
    public partial class UpdateConsumer : System.Web.UI.UserControl
    {
        private int id = 0;
        Consumer entity = null;
        private static readonly IAccessControlService accessControl = new AccessControlDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (((SiteMaster)Page.Master) != null)
            {
                ((SiteMaster)Page.Master).PageTitle = "Update Consumer";
            }

            if (Request.QueryString != null && Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
                entity = accessControl.Get(id);
            }
            
            if (!Page.IsPostBack)
            {
                string[] names = Enum.GetNames(typeof(METHODS));

                for (int i = 0; i < names.Length; i++)
                {
                    ListItem item = new ListItem(names[i], names[i]);
                    chkMethodAcces.Items.Add(item);
                }
                RenderEntity();
            }            
        }

        private void RenderEntity()
        {            
            if (entity != null)
            {
                lbName.Text = entity.Name;
                lbAuthKey.Text = entity.AuthKey;

                rdActive.Checked = entity.Active;
                rdAllowAll.Checked = entity.AllowAll;

                //method access
                foreach (ListItem cBox in chkMethodAcces.Items)
                {
                    if (entity.MethodAccess != null)
                    {
                        if (entity.MethodAccess.Contains(cBox.Value))
                        {
                            cBox.Selected = true;
                        }
                    }
                }

                //access items
                if(entity.AccessItems != null)
                {
                    foreach(AccessItem itm in entity.AccessItems)
                    {
                        ltrlAccessItems.Text += CreateAccessLine(itm.Sport, itm.Tournament);
                    }
                }
            }
        }

        private string CreateAccessLine(string sport, string tournament)
        {
            string lineItem = "<div><span>Sport: </span><input type=\"text\" name=\"txSport\" id=\"txSport\" value=\"" + sport + "\" ><span>Tournament: </span><input type=\"text\" name=\"txTournament\" id=\"txTournament\" value=\"" + tournament + "\" ></div>";
            return lineItem;
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Consumer c = entity;

            c.Active = rdActive.Checked;
            c.AllowAll = rdAllowAll.Checked;
            if (c.MethodAccess == null)
            {
                c.MethodAccess = new List<string>();
            }

            foreach (ListItem cBox in chkMethodAcces.Items)
            {
                if (cBox.Selected)
                {
                    c.MethodAccess.Add(cBox.Value);
                }
            }

            AccessItem ac = new AccessItem();
            if (c.AccessItems == null)
            {
                c.AccessItems = new List<AccessItem>();
            }

            c.AccessItems = new List<AccessItem>();
            if (this.Request.Form.Get("txSport") != null)
            {
                string[] sports = this.Request.Form.Get("txSport").Split(',');
                string[] tournaments = this.Request.Form.Get("txTournament").Split(',');

                int i = 0;
                foreach (string s in sports)
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        AccessItem acitem = new AccessItem();
                        acitem.Sport = s;
                        if (!string.IsNullOrEmpty(tournaments[i]))
                        {
                            acitem.Tournament = tournaments[i];
                            c.AccessItems.Add(acitem);
                        }
                    }
                    i++;
                }
            } 

            accessControl.Update(c);
            Response.Redirect("ConsumerList.aspx");
        }
    }
}