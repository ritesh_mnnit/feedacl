﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="FeedACL._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<div class="mid-container wrapper">

<h1 class="title">Sign In</h1>

    <asp:Panel ID="pnlAlert" runat="server" CssClass="alert alert-error" Visible="false"><asp:Label ID="lbErrorMessage" runat="server" Text="Login failed" Visible="false"></asp:Label></asp:Panel>

<ul class="fields">
      <li>
        <i class="icon-user"></i>
        <asp:TextBox id="tbUsername" runat="server" CssClass="edit_stretch_noborder" placeholder="username"></asp:TextBox>
      </li>
      <li>
        <i class="icon-key"></i>
        <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" placeholder="password"></asp:TextBox>
      </li>
</ul>
<div class="mid-form">
<asp:Button ID="btLogin" runat="server" Text="LOGIN" 
                     onclick="btLogin_Click" CssClass="btn blue large pull-right" />
                     </div>
</div>
</asp:Content>
