﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperSport.DBUtility;
using System.Data.SqlClient;
using System.Data;
using SuperSport.Utilities.Json;

namespace SuperSport.FeedACLLib.DAL
{
    public class AccessControlDAL : IAccessControlService
    {
        private const string SQL_CREATE = "INSERT INTO SuperSport.dbo.FeedACL (name, authkey, data) VALUES (@name, @authkey, @data)";
        private const string SQL_UPDATE = "UPDATE SuperSport.dbo.FeedACL set name=@name, authkey=@authkey, data=@data WHERE id=@id";
        //private const string SQL_DELETE = "DELETE FROM FeedACL WHERE id = @id";
        private const string SQL_GET_ONE = "SELECT * FROM SuperSport.dbo.FeedACL WHERE id = @id";
        private const string SQL_GET_ONE_BY_AUTHKEY = "SELECT * SuperSport.dbo.FROM FeedACL WHERE authkey = @authkey";
        private const string SQL_GET_ALL = "SELECT * FROM SuperSport.dbo.FeedACL";

        public Models.Consumer Create(Models.Consumer entity)
        {
            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@name", entity.Name));
            parm.Add(new SqlParameter("@authkey", entity.AuthKey));
            parm.Add(new SqlParameter("@data", entity.ToJSON()));

            SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_CREATE, parm.ToArray());

            return entity;
        }

        public Models.Consumer Update(Models.Consumer entity)
        {
            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@id", entity.Id));
            parm.Add(new SqlParameter("@name", entity.Name));
            parm.Add(new SqlParameter("@authkey", entity.AuthKey));
            parm.Add(new SqlParameter("@data", entity.ToJSON()));

            SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_UPDATE, parm.ToArray());

            return entity;
        }

        public Models.Consumer Get(int id)
        {
            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@id", id));

            Models.Consumer c = new Models.Consumer();

            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_GET_ONE, parm.ToArray()))
            {
                while (rdr.Read())
                {
                    string data = Convert.ToString(rdr["data"]);
                    c = c.FromJSON<Models.Consumer>(data);
                    c.Id = Convert.ToInt32(rdr["id"]);
                }
            }

            return c;
        }

        public List<Models.Consumer> Get()
        {
            List<Models.Consumer> consumers = new List<Models.Consumer>();

            List<SqlParameter> parm = new List<SqlParameter>();

            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_GET_ALL, parm.ToArray()))
            {
                while (rdr.Read())
                {
                    Models.Consumer c = new Models.Consumer();

                    string data = Convert.ToString(rdr["data"]);
                    c = c.FromJSON<Models.Consumer>(data);
                    c.Id = Convert.ToInt32(rdr["id"]);

                    consumers.Add(c);
                }
            }
            return consumers;
        }

        private void Validate(Models.Consumer entity)
        {
            if (entity == null)
            {
                throw new Exception("Consumer null or empty");
            }

            if (string.IsNullOrEmpty(entity.Name))
            {
                throw new Exception("Consumer name can't be empty");
            }
        }


        public Models.Consumer GetByAuthKey(string authKey)
        {
            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@authkey", authKey));

            Models.Consumer c = new Models.Consumer();

            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_GET_ONE, parm.ToArray()))
            {
                while (rdr.Read())
                {
                    string data = Convert.ToString(rdr["data"]);
                    c = c.FromJSON<Models.Consumer>(data);
                    c.Id = Convert.ToInt32(rdr["id"]);
                }
            }

            return c;
        }
    }
}
