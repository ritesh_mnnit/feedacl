﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperSport.FeedACLLib.Models;

namespace SuperSport.FeedACLLib
{
    public interface IAccessControlService
    {
        Consumer Create(Consumer entity);
        Consumer Update(Consumer entity);
        Consumer Get(int id);
        Consumer GetByAuthKey(string authKey);
        List<Consumer> Get();
    }
}
