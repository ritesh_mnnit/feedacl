﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperSport.FeedACLLib.Models
{
    public class AccessItem
    {
        public string Sport { get; set; }
        public string Tournament { get; set; }
        public List<string> MethodAccess { get; set; }
    }
}
