﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperSport.FeedACLLib.Models
{
    public class Consumer
    {
        public int Id { get; set; }
        public bool Active { get; set; }
        public string Name { get; set; }
        public string AuthKey { get; set; }
        public bool AllowAll { get; set; }
        
        public List<string> MethodAccess { get; set; }
        public List<AccessItem> AccessItems { get; set; }
    }
}
